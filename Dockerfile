FROM node:14-alpine

WORKDIR /usr/src/app

COPY . .

RUN npm install --only=dev
RUN npm install

CMD npm start
