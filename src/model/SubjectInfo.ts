import mongoose from "mongoose";
import { ISubjectInfo } from "./SubjectInfo.interface.js";

const { Schema, model } = mongoose;
const subjectInfoSchema = new Schema({
  _id: {
    type: String,
    required: true
  },
  name: {
    type: String,
    required: true
  },
  department: {
    type: String
  },
  credits: {
    type: Number,
    required: true
  }
});

export default model<ISubjectInfo>('SubjectInfo', subjectInfoSchema);
