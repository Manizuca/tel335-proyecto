import mongoose from "mongoose";

interface IRefSubjectClass {
  slug: string;
  campus: string;
  prefNumber?: number;
  options?: number[];
}

interface IUserSchedule extends mongoose.Document {
  name: string;
  semester: number;
  classes?: IRefSubjectClass[];
}

interface AddClassInput {
  slug: string;
  campus: string;
  classNum: number;
  isOption: boolean;
}

export { IRefSubjectClass, IUserSchedule, AddClassInput };
