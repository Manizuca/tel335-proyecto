import mongoose from "mongoose";

interface ISubjectInfo extends mongoose.Document {
  name: string;
  department: string;
  credits: number
}

export { ISubjectInfo };
