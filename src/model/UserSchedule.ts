import mongoose from "mongoose";
import { IUserSchedule } from "./UserSchedule.interface.js"


const { Schema, model } = mongoose;

const refSubjectClassSchema = new Schema({
  slug: {
    type: String,
    required: true,
    minlength: 1,
    maxlength: 10
  },
  campus: {
    type: String,
    required: true,
    minlength: 1
  },
  prefNumber: {
    type: Number
  },
  options: {
    type: [Number]
  }
});

const userScheduleSchema = new Schema({
  name: {
    type: String,
    required: true,
    minlength: 3
  },
  semester: {
    type: Number,
    required: true,
    min: 20200
  },
  classes: {
    type: [refSubjectClassSchema]
  }
});

export default model<IUserSchedule>('UserSchedule', userScheduleSchema)
