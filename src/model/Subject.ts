import mongoose from "mongoose";
import { ISubject } from "./Subject.interface.js";

const { Schema, model } = mongoose;

const timeSlotSchema = new Schema({
  day: {
    // Sun = 0, Mon = 1, ..., Sat = 6
    type: Number,
    required: true
  },
  slot: {
    type: [Number],
    required: true
  }
},{ _id : false });

const classSchema = new Schema({
  number: {
    type: Number,
    required: true
  },
  teacher: {
    type: String,
    required: true
  },
  timeSlots: {
    type: [timeSlotSchema],
    required: true
  }
},{ _id : false });

const subjectSchema = new Schema({
  semester: {
    // 20201, 20202, 20212, etc
    type: Number,
    required: true
  },
  slug: {
    type: String,
    required: true
  },
  campus: {
    type: String,
    required: true
  },
  classes: {
    type: [classSchema]
  }
});

export default model<ISubject>('Subject', subjectSchema);
