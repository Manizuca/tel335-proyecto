import mongoose from "mongoose";

interface ITimeSlot {
  day: number;
  slot: number[];
}

interface IClass {
  number: number;
  teacher: string;
  timeSlots: ITimeSlot[];
}

interface ISubject extends mongoose.Document {
  semester: number;
  slug: string;
  campus: string;
  classes: IClass[];
}

interface AddSubjectClassInput {
  semester: number;
  slug: string;
  campus: string;
  class: IClass;
}

export { ITimeSlot, IClass, ISubject, AddSubjectClassInput };
