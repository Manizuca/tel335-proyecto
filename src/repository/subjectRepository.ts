import { ITimeSlot, IClass, ISubject } from "../model/Subject.interface.js";
import SubjectInfo from "../model/SubjectInfo.js";
import Subject from "../model/Subject.js";

export const insertSubjectClass = async (semester: number, slug: string,
                            campus: string, number: number, teacher: string,
                            timeSlots: ITimeSlot[]) => {
  const subjectInfo = await SubjectInfo.findOne({ _id: slug });
  if (!subjectInfo) {
    return null;
  }

  const subject: ISubject = await Subject.findOneAndUpdate(
    { semester, slug, campus },
    {},
    { upsert: true, new: true }
  );
  const exists = subject.classes.some((_class: IClass) => _class.number == number);

  let updatedSubject;
  if (exists) {
    updatedSubject = await Subject.findOneAndUpdate(
      { _id: subject._id },
      { $set: {
        "classes.$[class].teacher": teacher,
        "classes.$[class].timeSlots": timeSlots
      }},
      { arrayFilters: [{ "class.number": number }], new: true }
    );
  } else {
    updatedSubject = await Subject.findOneAndUpdate(
      { _id: subject._id },
      { $push: { classes: { number, teacher, timeSlots } } },
      { new: true }
    );
  }
  return updatedSubject && updatedSubject.classes;
};

export const requestSubjectClasses = async (slug: string, semester: number, campus: string) => {
  const subject: ISubject | null = await Subject.findOne({ campus, slug, semester });
  return subject && subject.classes;
}
