import mongoose from "mongoose";
import { ITimeSlot, IClass } from "../model/Subject.interface.js";
import { IRefSubjectClass, IUserSchedule } from "../model/UserSchedule.interface.js";
import UserSchedule from "../model/UserSchedule.js"
import { requestSubjectClasses } from "../repository/subjectRepository.js"

interface SubjectClass {
  slug: string;
  campus: string;
  number: number;
  isOption: boolean;
  teacher: string;
  timeSlots: ITimeSlot[];
}

interface UserScheduleResponse {
  name: string;
  semester: number;
  classes?: SubjectClass[];
}

const resolveUserSchedule = async (userSchedule: IUserSchedule): Promise<UserScheduleResponse | null> => {
  const classes: SubjectClass[] = [];

  if (userSchedule.classes !== undefined) {
    const promises = userSchedule.classes.map(async (subject) => {
      const subjectClasses: IClass[] | null = await requestSubjectClasses(subject.slug, userSchedule.semester, subject.campus);
      if (!Array.isArray(subjectClasses) || subjectClasses.length <= 0) return;

      if (subject.prefNumber) {
        const thisClass: IClass | undefined = subjectClasses.find((thisClass) => thisClass.number === subject.prefNumber);
        thisClass && classes.push({
          slug: subject.slug, campus: subject.campus, isOption: false,
          number: thisClass.number, teacher: thisClass.teacher, timeSlots: thisClass.timeSlots
        });
      }

      if (Array.isArray(subject.options)) {
        let index = -1;
        const thisClasses: IClass[] = subjectClasses.filter((thisClass) => {
          index = subject.options!.indexOf(thisClass.number);
          if (index >= 0) {
            subject.options!.splice(index, 1);
            return true;
          }
          return false;
        });

        thisClasses.forEach((thisClass) => classes.push({
          slug: subject.slug, campus: subject.campus, isOption: true,
          number: thisClass.number, teacher: thisClass.teacher, timeSlots: thisClass.timeSlots
        }));
      }
    });

    await Promise.all(promises);
  }

  return { name: userSchedule.name, semester: userSchedule.semester, classes: classes.length > 0 ? classes : undefined };
}

export const insertUserSchedule = async (name: string, semester: number): Promise<IUserSchedule> => {
  const userSchedule: IUserSchedule = await UserSchedule.create({name, semester});
  return userSchedule;
}

export const requestUserSchedule = async (name: string, semester: number): Promise<UserScheduleResponse | null> => {
  const userSchedule: IUserSchedule | null = await UserSchedule.findOne({ name, semester });
  const response = userSchedule && (await resolveUserSchedule(userSchedule));

  return response;
}

export const pushClass = async (name: string, semester: number, slug: string, campus: string, classNum: number, isOption: boolean): Promise<UserScheduleResponse | null> => {
  const schedule: IUserSchedule | null = await UserSchedule.findOne({ name, semester });
  if (!schedule){
    return null;
  }
  const existingSubject: IRefSubjectClass | undefined = schedule.classes &&
    schedule.classes.find((_class) => _class.slug == slug && _class.campus == campus);

  const modifier: mongoose.UpdateQuery<IUserSchedule> = { }
  const opts: mongoose.QueryFindOneAndUpdateOptions = { new: true };

  if (!existingSubject) {
    if (isOption){
      modifier.$push = { classes: { slug, campus, options: [classNum] } };
    } else {
      modifier.$push = { classes: { slug, campus, prefNumber: classNum } };
    }
  } else {
    opts.arrayFilters = [{ "item.slug": slug, "item.campus": campus }];
    if (isOption) {
      modifier.$addToSet = { "classes.$[item].options": classNum };
      if (existingSubject.prefNumber == classNum) {
        modifier.$unset = { "classes.$[item].prefNumber": "" };
      }
    } else {
      modifier.$pull = { "classes.$[item].classes": classNum };
      modifier.$set = { "classes.$[item].prefNumber": classNum };
      if (existingSubject.prefNumber) {
        modifier.$addToSet = { "classes.$[item].options": existingSubject.prefNumber };
      }
    }

  }

  const userSchedule: IUserSchedule | null = await UserSchedule.findOneAndUpdate(
    { _id: schedule._id },
    modifier,
    opts
  );

  const response = userSchedule && (await resolveUserSchedule(userSchedule));
  return response;
}
