import { ISubjectInfo } from "../model/SubjectInfo.interface.js";
import SubjectInfo from "../model/SubjectInfo.js";

export const insertSubjectInfo = async (slug: string, name: string,
                            department: string, credits: number): Promise<ISubjectInfo> => {
  const subjectInfo: ISubjectInfo = await SubjectInfo.findOneAndUpdate(
    { _id: slug },
    { name, department, credits },
    { upsert: true, new: true }
  );

  return subjectInfo;
};

export const requestSubjectsInfo = async () => {
  const subjectsInfo: ISubjectInfo[] = await SubjectInfo.find();
  return subjectsInfo;
}
