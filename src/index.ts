import cors from "cors";
import http from "http";
import express from "express";
import mongoose from "mongoose";

// Controllers
import { addSubjectInfo, getSubjectsInfo } from "./controller/subjectInfoController.js";
import { addSubjectClass, getSubjectClasses } from "./controller/subjectController.js";
import { addClass, addUserSchedule, getUserSchedule } from "./controller/userScheduleController.js";

const port: number = 3000;

interface ExpressApp extends express.Express {
  server?: http.Server;
}

export default function run_server(testing: boolean | undefined): ExpressApp {
  if (!testing) {
    mongoose.connect('mongodb://localhost:27017/tel335_proyecto', {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
      useFindAndModify: false
    });
  }

  const app: ExpressApp = express();
  app.use(cors());
  app.use(express.json());
  app.use(express.urlencoded({ extended: false }));

  app.post("/subject", addSubjectInfo);
  app.post("/subject/:slug", addSubjectClass);
  app.post("/schedule/:name", addUserSchedule);
  app.post("/schedule/:name/:semester", addClass);

  app.get("/subject", getSubjectsInfo);
  app.get("/subject/:slug", getSubjectClasses);
  app.get("/schedule/:name", getUserSchedule);

  const usedPort = testing ? (port + 1) : port;
  app.server = app.listen(usedPort,
    () => console.log(`Listening to port ${usedPort}`)
  );
  return app;
}
