import { Request, Response } from "express";
import { pushClass, requestUserSchedule, insertUserSchedule } from "../repository/userScheduleRepository.js"
import { AddClassInput } from "../model/UserSchedule.interface.js";

export const addUserSchedule = async (
      req: Request<{ name: string }, {}, { semester: number }>,
      res: Response
    ) => {
  const { name } = req.params;
  const { semester } = req.body;

  try {
    const userSchedule = await insertUserSchedule(name, semester );
    res.status(200).json(userSchedule);
  } catch (err) {
    res.status(500).json({
      message: "There was an error adding the user schedule: " + err.message
    });
  }
}

export const getUserSchedule = async (
      req: Request<{ name: string }, {}, {}, { semester: number }>,
      res: Response
    ) => {
  const { name } = req.params;
  const { semester } = req.query;

  try {
    const userSchedule = await requestUserSchedule(name, semester);
    if (!userSchedule) {
      res.status(404).json({ message: "There is no user schedule" });
    } else {
      res.status(200).json(userSchedule);
    }
  } catch (err) {
    res.status(500).json({
      message: "Internal Error: " + err.message
    });
  }
}

export const addClass = async (
      req: Request<{ name: string, semester: number }, {}, AddClassInput>,
      res: Response
    ) => {
  const { name, semester} = req.params;
  const { slug: unsanitizedSlug, campus, classNum, isOption } = req.body;

  try {
    const slug = unsanitizedSlug.toUpperCase().replace("-", "");

    const userSchedule = await pushClass(name, semester, slug, campus, classNum, isOption);
    if(!userSchedule) {
      res.status(404).json({ message: "There is no user schedule" });
    } else {
      res.status(200).json(userSchedule);
    }
  } catch (err) {
    res.status(500).json({
      message: "Internal Error: " + err.message
    })
  }
}
