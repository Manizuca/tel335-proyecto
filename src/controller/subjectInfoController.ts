import { Request, Response } from "express";
import { insertSubjectInfo, requestSubjectsInfo } from "../repository/subjectInfoRepository.js"
import { ISubjectInfo } from "../model/SubjectInfo.interface.js";

export const addSubjectInfo = async (
      req: Request<{}, {}, ISubjectInfo>,
      res: Response
    ) => {
  const { _id, name, department, credits } = req.body;

  try {
    const slug = _id.toUpperCase().replace("-", "");

    const subjectInfo = await insertSubjectInfo(slug, name, department, credits);
    res.status(200).json(subjectInfo);
  } catch (err) {
    res.status(500).json({
      message: "There was an error adding the subject: " + err.message
    });
  }
}

export const getSubjectsInfo = async (
      req: Request,
      res: Response
    ) => {
  try {
    const subjectsInfo = await requestSubjectsInfo();
    res.status(200).json(subjectsInfo);
  } catch (err) {
    res.status(500).json({
      message: "Internal error: " + err.message
    });
  }
}
