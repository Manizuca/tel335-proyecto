import { Request, Response } from "express";
import { insertSubjectClass, requestSubjectClasses } from "../repository/subjectRepository.js"
import { AddSubjectClassInput } from "../model/Subject.interface.js";

export const addSubjectClass = async (
      req: Request<{ slug: string }, {}, AddSubjectClassInput>,
      res: Response
    ) => {
  const { semester, campus, class: _class } = req.body;
  const { slug: unsanitizedSlug } = req.params;

  try {
    const slug = unsanitizedSlug.toUpperCase().replace("-", "");

    const subject = await insertSubjectClass(semester, slug, campus, _class.number, _class.teacher, _class.timeSlots);
    if (!subject){
      res.status(404).json({ message: "There is no subject with that slug" });
    } else {
      res.status(200).json(subject);
    }
  } catch (err) {
    res.status(500).json({
      message: "There was an error adding the class: " + err.message
    });
  }
}

export const getSubjectClasses = async (
      req: Request<{ slug: string }, {}, {}, { campus: string, semester: number }>,
      res: Response
    ) => {
  const { campus, semester } = req.query;
  const { slug: unsanitizedSlug } = req.params;

  try {
    const slug = unsanitizedSlug.toUpperCase().replace("-", "");

    const subject = await requestSubjectClasses(slug, semester, campus);
    if (!subject) {
      res.status(404).json({ message: "There is no classes for that subject" });
    } else {
      res.status(200).json(subject);
    }
  } catch (err) {
    res.status(500).json({
      message: "There was an error getting the classes: " + err.message
    });
  }
}
