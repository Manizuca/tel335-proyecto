import supertest from "supertest";
import runServer from "../build/index.js";

mongoose.connect('mongodb://localhost:27017/tel335_proyecto_test', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true
});

const app = runServer(true);
const request = supertest(app.server);

afterAll(
  () => app.server.close(
    () => mongoose.connection.db.dropDatabase(
      () => mongoose.disconnect()
    )
  )
);
